package mychar;

public class MyChar {
    private char ch;
    public MyChar(char ch)
    {
        this.ch=ch;
    }
    public boolean isVowel()
    {
        if(ch== 'a' || ch=='e' || ch=='i' || ch== 'o' || ch== 'u')
            return true;

        return false;
    }

    public boolean isConsonant() {
        if(!isVowel())
            return true;

        return false;
    }
}
